﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace WebApiTest.Controllers {

    // Controller that controls data of To do items
    public class TodoListController : ApiController {

        // List to store To do items
        static List<string> data = new List<string>();

		// Default constructor
		public TodoListController() {

            // Add dummy data
            data.Add("Test item 1");
            data.Add("Test item 2");
        }

        // GET method that returns the list of To do items
        // GET todolist
        public List<string> Get() {
            return data;
        }

		// GET method that returns the To do item at the given index
        // GET todolist/{index}
		public HttpResponseMessage Get(int index) {

            // If a valid index is passed return the appropriate item for the index
            if(index >= 0 && index < data.Count) {
                return Request.CreateResponse<string>(HttpStatusCode.OK, data[index]);
            }

            // If an invalid index is passed return a 404 error
            return Request.CreateErrorResponse(HttpStatusCode.NotFound, "Item not found");
		}

        // POST method for adding new To do item to list
        // POST todolist
        public HttpResponseMessage Post([FromBody]string value) {
            
            data.Add(value);

            // Index of newly added item
            int indexOfItem = data.Count - 1;

            // Create a response to return to the client
            var response = Request.CreateResponse(HttpStatusCode.OK);
			
            // Return the URI of the new item in Headers.Location
			response.Headers.Location = new Uri(Request.RequestUri + "/" + indexOfItem.ToString());

            return response;
        }

        // DELETE method for deleting the To do item at the given index
        // DELETE todolist/{index}
        public HttpResponseMessage Delete(int index) {

			// If a valid index is passed delete the appropriate item for the index
			if (index >= 0 && index < data.Count) {

                // Remove the item at the requested index
                data.RemoveAt(index);

                // Request has valid so return a 200
                return Request.CreateResponse(HttpStatusCode.OK);
            }

			// If an invalid index is passed return a 404 error
			return Request.CreateErrorResponse(HttpStatusCode.NotFound, "Invalid index");
        }


    }

}
